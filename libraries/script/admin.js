var q = jQuery.noConflict();
var MD = {


    /**
     * Define global variables
     * accessable
     */
    global: {
        
        address            : '',
        map                : null,
        center_location    : [],
        attractions        : [],
        attractions_markers: [],
        location_index     : -1,
        location_marker    : null,
        map_zoom           : 14,
        active_sub_panel   : 'default-panel',
        default_content    : `
            <div class="content-default">
                <h3>You did not set content for this location. Enter you content on the "Location Content" panel.</h3>        
            </div>
        `
    },


    /**
     * Build map
     */
    build: function() {        
        MD.handlers();
    },
    


    /**
     * Communicate with wordpress api
     * @param {json} data 
     */
    map_server: function(data, callback, is_return = false) {  
        q.ajax({
            url     : map_api.url,
            type    : 'POST',
            data    : data,
            cache   : false,
            dataType: 'json'
        }).done(function(response) {            
            if (is_return)
                callback(response);
        });
    },



    /**
     * Get main address
     * @param callback 
     */
    get_address_location: function(callback) {
        
        if (MD.global.attractions.length <= 0) {   
            q(created_attractions).each(function(index, destination) {
                if (destination.title === '')
                    created_attractions.splice( index, 1 );    
            });         
            MD.global.attractions = created_attractions;
        }

        var addr = q('#map-address').val();
        MD.global.address = addr.replace(/(\r\n|\n|\r)/gm, " ");    
        
        (MD.global.address !== '')?
            
            MD.map_server({
                action: 'address',
                address: MD.global.address
            }, function(response) {
                console.log(response);
                callback(response.latlng);
            },true)

        :
            q('#map-address').focus()  
        ;
    },



    /**
     * Create map
     * @param callback 
     */
    map_settings: function(callback) {
        MD.get_address_location(function(latlng) {
             
            if (latlng[0] === null) return;

            MD.global.center_location = latlng;

            var base = {    
                'OpenStreetMap': L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    'attribution': 'Map data &copy; OpenStreetMap contributors'
                })
            };

            if (MD.global.map !== null)
                MD.global.map.remove();
            
            
            MD.global.map = L.map('map-canvas', {
                'center': latlng,
                'zoom'  : MD.global.map_zoom,
                'layers': [
                    base.OpenStreetMap
                ]
            }).on('click', function(e) { 
                
                if (Array.isArray(MD.global.attractions) === false)
                    MD.global.attractions = [];
                
                MD.global.attractions.push({
                    latlng: [e.latlng.lat, e.latlng.lng],
                    title: '',
                    content: ''
                });
                MD.display_distinations();   
                MD.save();

            });
            

            callback();
        });  
    },



    /**
     * Control conditional fields
     */
    map_fields: function(status) {
        (status)?
            q('.map-conditional')
                .removeClass('hide')
        :
            q('.map-conditional')
                .addClass('hide')
        ;
        
    },



    map_field_set: function(callback) {
        
        var m = MD.global.attractions[MD.global.location_index];
        var editor = tinyMCE.get('map-location-content');
        
        if (editor === null) {
            q('#map-location-content').val(m.content);
        } else {

            q('#location-title').val('');
            editor.setContent('');

            if ('' !== m.content)
                editor.setContent(m.content);

        }

        if ('' !== m.title)
            q('#location-title').val(m.title);        

        callback();

    },



    /**
     * Play video on click
     * location
     * @param {*} index 
     * @param {*} marker 
     */
    play_video: function() {
        var iframe = q(".attraction-content").find('iframe');
        var src    = iframe.attr('src');
        var url    = src;


        if (typeof(src) !== 'undefined' && src.includes('?')) {
            src = src.split('?');
            url = src[0] + '?autoplay=1'                    
        }
        iframe.attr('src', url);
    },



    /**
     * Center map view via marker
     * @param {array index} index 
     * @param {active marker} marker 
     */
    map_location_settings: function(index, marker) {

        MD.global.location_marker = marker;
        MD.global.location_index  = index;
        
        MD.map_field_set(function() {

            MD.global.map.setView(marker.getLatLng(), MD.global.map_zoom);
            MD.map_fields(true);
            MD.global.active_sub_panel = 'marker';
            q('.panel-nav[data-target="location"]').trigger('click');
            MD.play_video();

            q('.map-sub-panel').animate({
                scrollTop: q('#location-title').offset().top - 80
            }, 1000);

            q('#location-title').focus();
            q('.map-plot-section').hide();            

        });
    },


    /**
     * Method to display 
     * created attractions on the map
     */
    display_distinations: function() {

        var attractions = '';
        console.log(MD.global.attractions);
        MD.global.attractions_markers = [];

        q(MD.global.attractions).each(function(index, destination) {

            // don't add marker if empty
            if ('' === destination.content) return true;
            
            if (typeof(destination.latlng) === 'undefined') return true;

            var m = L.marker(destination.latlng, {icon: leaflet_marker}).addTo(MD.global.map);

            var content = `
            <h2>`+ destination.title +`</h2>
            <div class="attraction-content">`+ destination.content +`</div>
            `;

            if ('' !== destination.title) {
                attractions += `
                <a href="#" class="shortcut-marker" data-index="`+ index +`">
                    <h2>`+ destination.title +`</h2>
                </a>
                `;
            }


            // if ('' === destination.content)
            //     content = MD.global.default_content;

            m
                .bindPopup( content )
                .openPopup()
                .on('click', function(e) {
                    MD.map_location_settings(index, e.target);                    
                });
            
            
            MD.global.attractions_markers.push(m);
            
        });

        MD.global.map.closePopup();
        MD.global.map.doubleClickZoom.disable(); 

        q('.map-attractions').html(attractions);

        if (MD.global.location_index >= 0) {

            try {
                MD.global.attractions_markers[MD.global.location_index].openPopup();
                MD.global.map.setView(
                    MD.global.attractions_markers[MD.global.location_index].getLatLng(), 
                    MD.global.map_zoom
                );        
            } catch(err) {}

            q('.shortcut-marker').removeClass('active');
            q('.shortcut-marker[data-index="'+ MD.global.location_index +'"]')
                .addClass('active');        

            MD.play_video();    
        }

    },



    /**
     * Sidebar navigations
     * @param {event} e 
     */
    panel_nav: function(e) {
        e.preventDefault();        

        var panel = q(this).data('target');    

        q('.panel-content')
            .removeClass('show')
            .addClass('hide');
        q('.map-panel-' + panel)
            .removeClass('hide')
            .addClass('show');
            q('.map-panel-' + panel + ' input:first').focus();

        q('.panel-nav').removeClass('active');
        q(this).addClass('active');    
        

        var is_attractions = true;
        if ('settings' === q(this).data('target'))
            is_attractions = false;

        MD.update_map_dimension(is_attractions);    
        
    },    



    /**
     * Quick shortcut too add marker form
     *
     * @param   {[type]}  e  [e description]
     *
     * @return  {[type]}     [return description]
     */
    quick_add_marker: function(e) {
        e.preventDefault();
        MD.map_fields(false);
        q('.toggle-marker').trigger('click');
    },



    /**
     * Save map settings
     * @param {event} e 
     */
    save_map_settings: function(e) {
        e.preventDefault();

        MD.global.address = q('#map-address').val();
        
        MD.map_settings(MD.display_distinations);

        MD.save();
    },    



    /**
     * Plot entered address
     *
     * @param   {[type]}  e  [e description]
     *
     * @return  {[type]}     [return description]
     */
    plot_address: function(e) {
        e.preventDefault();
        
        MD.map_fields(false);

        var addr = q('#search-address-field').val();
            addr = addr.replace(/(\r\n|\n|\r)/gm, " ");   
            
        var addr_lat = q('#addr-lat').val();
        var addr_lng = q('#addr-lng').val();
         
        
        if ('' !== addr_lat && '' !== addr_lng) {

            if (Array.isArray(MD.global.attractions) === false)
                MD.global.attractions = [];
            
            MD.global.attractions.push({
                latlng : [addr_lat, addr_lng],
                title  : 'No Title',
                content: 'You did not set content for this location. Enter you content on the "Location Content" panel.'
            });

            MD.global.location_index = MD.global.attractions.length - 1;

            MD.display_distinations();   
            MD.save();

            q('.map-plot-section').hide();
            q('.map-plot-section input, .map-plot-section textarea').val('');
            MD.map_fields(true);


        } else if ('' !== addr) {
            MD.map_server({
                action : 'address',
                address: addr
            }, function(response) {
                if (Array.isArray(MD.global.attractions) === false)
                    MD.global.attractions = [];
                
                MD.global.attractions.push({
                    latlng : response.latlng,
                    title  : addr,
                    content: 'You did not set content for this location. Enter you content on the "Location Content" panel.'
                });
    
                MD.global.location_index = MD.global.attractions.length - 1;
    
                MD.display_distinations();   
                MD.save();
    
                q('.map-plot-section').hide();
                q('.map-plot-section input, .map-plot-section textarea').val('');
                MD.map_fields(true);
    
            },true);     
        }
    },



    /**
     * Update map dimensions
     * when navigations triggered
     * @param {boolean} is_attractions 
     */
    update_map_dimension: function(is_attractions) {

        q('.map-toggle-nav a')
                .removeClass('active');

        if ('attractions' === MD.global.active_sub_panel)        
            q('.map-toggle-nav a:first').addClass('active');
        else if ('marker' === MD.global.active_sub_panel)        
            q('.map-toggle-nav a:last').addClass('active');


        var canvas = 0;
        var w_width = q(window).width();

        if (is_attractions) {
            q('.map-sub-panel').addClass('hide');
            q('.' + MD.global.active_sub_panel).removeClass('hide');

            canvas = q('.map-panel').outerWidth() + q('.' + MD.global.active_sub_panel).outerWidth();        
            
        } else {
            q('.map-sub-panel').addClass('hide');
            canvas = q('.map-panel').outerWidth();
        }

        w_width -= canvas;

        q('#map-canvas').width( w_width );  

        if (MD.global.map !== null)
            MD.global.map.invalidateSize();


    },



    /**
     * Remove selected marker
     * @param {event} e 
     */
    remove_marker: function(e) {
        e.preventDefault();

        if (confirm('Remove this marker?')) {
            MD.map_fields(false);
            MD.global.attractions_markers.splice( MD.global.location_index, 1 );
            MD.global.attractions.splice( MD.global.location_index, 1 );    
            MD.map_settings(MD.display_distinations);
            MD.save();

            q('.map-plot-section input, .map-plot-section textarea').val('');
            q('.map-plot-section').show();            
            MD.map_fields(false);
        }

    },    



    /**
     * Attractions tab navigation
     * handlers
     * @param {event} e 
     */
    nav_attractions: function(e) {
        e.preventDefault();

        if (q(this).hasClass('active')) {
            
            q('.map-toggle-nav a')
                .removeClass('active');

            MD.global.active_sub_panel = 'default-panel';                

        } else {

            q('.map-toggle-nav a')
                .removeClass('active');
            q('.map-toggle-nav a')
                .removeClass('active');
            q(this)
                .addClass('active');
            
            MD.global.active_sub_panel = q(this).data('target');

        }
        
        MD.update_map_dimension(true);

    },    



    /**
     * Send map settings and attractions
     * in the server
     */
    save: function() {
        MD.map_server({
            'action'     : 'save_attractions',
            'geolocation': JSON.stringify(MD.global.center_location),
            'address'    : MD.global.address,
            'attractions': JSON.stringify(MD.global.attractions)
        });
    },



    /**
     * Save created attractions
     * @param {event} e 
     */
    save_attraction_settings: function(e) {
        e.preventDefault();

        var content = '';
        var editor = tinyMCE.get('map-location-content');

        var title = q('#location-title').val();
        
        if (editor === null)
            content = q('#map-location-content').val();
        else
            content = editor.getContent();

        MD.global.attractions[MD.global.location_index].title = title;
        MD.global.attractions[MD.global.location_index].content = content;    
        
        MD.map_settings(MD.display_distinations); 

        MD.save();

        MD.map_fields(true);
        q('.map-plot-section').hide();

    },    


    /**
     * Shortcut to marker
     * @param {event} e 
     */
    shortcut_marker: function(e) {
        e.preventDefault();
        MD.global.location_index = parseInt(q(this).data('index'));
        MD.map_settings(MD.display_distinations);
    },



    /**
     * Map handlers     
     */
    handlers: function() {

        q(document).on('click', '.panel-nav', MD.panel_nav);

        q(document).on('click', '.button-add-new-marker', MD.quick_add_marker);

        q(document).on('click', '.save-map-settings', MD.save_map_settings);

        q(document).on('click', '.remove-marker', MD.remove_marker);

        q(document).on('click', '.map-toggle-nav a', MD.nav_attractions);

        q(document).on('click', '.save-location-settings', MD.save_attraction_settings);

        q(document).on('click', '.shortcut-marker', MD.shortcut_marker);

        q(document).on('click', '.button-plot-address', MD.plot_address);

        q(document).on('click', '.close-marker', function(e) {
            e.preventDefault();
            MD.map_fields(false);
            q('.map-plot-section').show();
        });

        q(window).on('resize', function() {
            MD.map_settings(MD.display_distinations);
            MD.update_map_dimension(true);
        }).resize();        

    }
    

};
MD.build();