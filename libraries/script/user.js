var q = jQuery.noConflict();
var MD = {

    /**
     * Define global variables
     * accessable
     */
    global: {
        address: '',
        map: null,
        attractions: [],
        attractions_markers: [],
        location_index: -1,
        zoom: 10,        
        user_marker: null,
        user_location: [],
        previous_positon: null,
        current_position: null,
        current_accuracy: null,
        state_interval: null,
        within_meters: 0.02,
        default_content: `
            <div class="content-default">
                <h3>This attraction has no content created.</h3>        
            </div>
            `
    },



    /**
     * Build map
     */
    build: function() {        
        MD.handlers();
    },



    /**
     * Get current user location
     * to plot on the map
     */
    get_user_current_location: function(callback) {
        navigator.geolocation.getCurrentPosition(function(position) {
            MD.global.user_location = [position.coords.latitude, position.coords.longitude]; 
            console.log(MD.global.user_location);                
        })
        callback(); 
    },



    /**
     * Get main address
     * @param callback 
     */
    get_address_location: function(callback) {
        
        if (false === Array.isArray(MD.global.attractions))
            MD.global.attractions = [];

        if (MD.global.attractions.length <= 0)
            MD.global.attractions = created_attractions;

        callback(JSON.parse(q('#map-geolocation').val()));
    },



    map_location_settings: function(index, marker) {
        MD.global.location_index = index;
        MD.global.map.setView(marker.getLatLng(), MD.global.zoom);
    },



    /**
     * Create map
     * @param callback 
     */
    map_settings: function(callback) {
        MD.get_address_location(function(latlng) {
            var base = {    
                'OpenStreetMap': L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                    'attribution': 'Map data &copy; OpenStreetMap contributors'
                })
            };

            if (MD.global.map !== null)
                map.remove();

            MD.global.map = L.map('map-canvas', {
                'center': latlng,
                'zoom': MD.global.zoom,
                'layers': [
                    base.OpenStreetMap
                ]
            });            

            callback();
        });  
    },



    /**
     * Method to display 
     * created attractions on the map
     */
    display_distinations: function() {

        MD.global.attractions_markers = [];

        q(MD.global.attractions).each(function(index, attraction) {
            
            var m = L.marker(attraction.latlng, {icon: leaflet_marker}).addTo(MD.global.map);           

            var content = `
            <h2>`+ attraction.title +`</h2>
            <div class="attraction-content">`+ attraction.content +`</div>
            `;


            if ('' === attraction.content)
                content = '';//default_content;

            m
                .bindPopup( content )
                .openPopup()
                .on('click', function(e) {

                    MD.display_popup( this, MD.play_video );
                    MD.global.map.closePopup();

                    //MD.map_location_settings(index, e.target);
                });
            
            
            MD.global.attractions_markers.push(m);
            
        });


        // MD.global.user_marker = L.marker(
        //     MD.global.user_location, {icon: user_marker}
        // )
        // .addTo(MD.global.map)
        // .bindTooltip('You are here')
        // .openTooltip();

        MD.global.map.closePopup();
        

    },



    /**
     * Play video on click
     * location
     * @param {*} index 
     * @param {*} marker 
     */
    play_video: function() {
        var iframe = q(".map-content-wrapper").find('iframe');
        var src    = iframe.attr('src');
        var url    = src;


        if (typeof(src) !== 'undefined' && src.includes('?')) {
            src = src.split('?');
            url = src[0] + '?autoplay=1'                    
        }
        iframe.attr('src', url);
    },



    /**
     * Update map dimensions
     * when navigations triggered
     * @param {boolean} is_attractions 
     */
    update_map_dimension: function(is_attractions) {

        var w_width = q(window).width();

        q('#map-canvas').width( w_width );  

        if (MD.global.map !== null)
            MD.global.map.invalidateSize();
    },



    /**
     * Check current user location
     * and draw a line
     */
    checkState: function() {
        //MD.global.state_interval = setInterval(function() {            
            MD.global.map.locate({
                setView: true,
                watch: true,
                maxZoom: MD.global.zoom,
                enableHighAccuracy: true,
                maximumAge: 2,
                timeout: 20000             
            });            
            MD.global.map.invalidateSize();
        //}, 500);
    },


    /**
     * Location found
     * popup marker content
     */
    location_found: function(e) {
        if (MD.global.current_position) {
            MD.global.map.removeLayer(MD.global.current_position);
            //MD.global.map.removeLayer(MD.global.current_accuracy);
        }
        
        var radius = e.accuracy / 2;
  
        MD.global.current_position = L.marker(e.latlng, {icon: user_marker}).addTo(MD.global.map);
          //.bindPopup("This is you.").openPopup();
        MD.global.map.flyTo(e.latlng, MD.global.zoom, {
            animate: false           
        });

  
        // MD.global.current_accuracy = L.circle(e.latlng, radius).addTo(MD.global.map);
        // console.log(MD.global.current_accuracy.getLatLng() );

        MD.popup_marker_content(e.latlng, radius);
    },


    /**
     * Get marker distance from
     * user current location     
     */
    get_distance: function(lat1, lon1, lat2, lon2, unit) {
        if ((lat1 == lat2) && (lon1 == lon2)) {
            return 0;
        }
        else {
            var radlat1 = Math.PI * lat1/180;
            var radlat2 = Math.PI * lat2/180;
            var theta = lon1-lon2;
            var radtheta = Math.PI * theta/180;
            var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
            if (dist > 1) {
                dist = 1;
            }
            dist = Math.acos(dist);
            dist = dist * 180/Math.PI;
            dist = dist * 60 * 1.1515;
            if (unit=="K") { dist = dist * 1.609344 }
            if (unit=="N") { dist = dist * 0.8684 }
            return dist;
        }
    },


    /**
     * Popup marker within
     * the uesr location's radius     
     */
    popup_marker_content: function(user_pos, radius) {        
        MD.global.attractions_markers.forEach(function(attraction) {
            
            var marker = attraction.getLatLng();

            var distance = MD.get_distance(
                marker.lat,
                marker.lng,
                user_pos.lat,
                user_pos.lng,
                'K'
            );            
            
            if (distance <= MD.global.within_meters) {            
                
                MD.display_popup( attraction, MD.play_video );

                MD.global.previous_positon = user_pos.lat;
                clearInterval(MD.global.state_interval);
            }

        });
    },


    /**
     * If error encountered during
     * loction search
     */
    location_error: function(e) {
        console.log(e.message);
    },


    display_popup: function( attraction, callback ) {
        
        MD.global.map.locate({
            setView: true,
            watch: false
        });

        q('.map-content-wrapper')
            .html( attraction.getPopup().getContent() )
            .append('<a href="#" class="close-popup-content">Close</a>');
        q('.map-marker-popup').show();

        callback();

    },


    /**
     * Update map interaction
     */
    map_interaction: function() {
        q('.map-marker-popup').hide();
        q('.map-content-wrapper').html('');

        MD.checkState();                
        MD.global.map.on('locationfound', MD.location_found);
        MD.global.map.on('locationerror', MD.location_error);
    },



    /**
     * Handlers
     */
    handlers: function() {

        q(window).on('resize', function() {
            MD.get_user_current_location(function() {
                MD.map_settings(MD.display_distinations);
                MD.update_map_dimension(true);
                MD.map_interaction();
            });            
        }).resize();

        q(document).on('click', '.close-popup-content', MD.map_interaction);

    }


};
MD.build();