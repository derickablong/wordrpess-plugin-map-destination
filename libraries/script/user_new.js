var map;
var marker;
var infoWindow;


var getCoor = function(position) {
    var pos = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
    };        

    infoWindow.setPosition(pos);
    infoWindow.setContent('Location found.');
    infoWindow.open(map);
    map.setCenter(pos);
};


var errorCoor = function() {
    handleLocationError(true, infoWindow, map.getCenter());
};


var map_initialize = function() {    

    var center = {lat: 9.306840, lng: 123.305450};

    //marker = new google.maps.Marker({position: uluru, map: map});

    map = new google.maps.Map(
        document.getElementById('map-canvas'), {zoom: 8, center: center});

    infoWindow = new google.maps.InfoWindow;

    navigator.geolocation.getCurrentPosition(
        getCoor, 
        errorCoor, 
        {maximumAge:60000, timeout:5000, enableHighAccuracy:true}
    );

};


var handleLocationError = function(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
                          'Error: The Geolocation service failed.' :
                          'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map);
};