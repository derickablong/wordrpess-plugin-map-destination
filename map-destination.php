<?php
/*
Plugin Name: Map Destination
Plugin URI: http://derickablong.com
description: Map destination
Version: 1.2
Author: Derick Ablong
Author http://derickablong.com
*/
define( 'MAP_DIR' , plugin_dir_path( __FILE__ ) );
define( 'MAP_URI', plugins_url() . '/map_destination' );

require_once MAP_DIR . '/includes/map-destination-leaflet.php';
require_once MAP_DIR . '/includes/map-destination-manager.php';

function run_map_destination() {

    $map_manager = new Map_Destination_Manager();
    $map_manager->run();

}

run_map_destination();