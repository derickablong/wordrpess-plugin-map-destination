<?php
/**
 * Module: Map Destination Loader
 * Description: Add hooks, filter 
 * and dependencies
 */
class Map_Destination_Loader {

    protected $actions;


    public function __construct() {
        $this->actions = array();
    }


    public function add_action( $hook, $component, $callback ) {
        $this->actions = $this->add( $this->actions, $hook, $component, $callback );
    }


    private function add( $hooks, $hook, $component, $callback ) {
        $hooks[] = array(
            'hook' => $hook,
            'component' => $component,
            'callback' => $callback
        );
        return $hooks;
    }


    /**
     * Build map loader
     * hooks and dependencies
     */
    public function run() {
        foreach ($this->actions as $action) {
            add_action( $action['hook'], array( $action['component'], $action['callback'] ) );
        }
    }

}