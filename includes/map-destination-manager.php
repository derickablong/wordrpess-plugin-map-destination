<?php
/**
 * Module: Map Destination Manager
 * Description: Map management for back-end and front-end
 * combination with leaflet
 */
class Map_Destination_Manager extends Map_Destination_Leaflet {

    protected $loader;

    protected $version;

    protected $textdomain;


    public function __construct() {

        $this->version = '1.0';
        $this->textdomain = 'map-destination';
        
        $this->load_dependencies();        
        $this->define_admin_hooks();
        $this->define_user_hooks();

    }


    private function load_dependencies() {

        require_once MAP_DIR . '/admin/map-destination-admin.php';
        require_once MAP_DIR . '/user/map-destination-user.php';
        require_once MAP_DIR . '/includes/map-destination-loader.php';
        $this->loader = new Map_Destination_Loader();

        parent::__construct( $this->loader, $this->version, $this->textdomain );


    }


    private function define_admin_hooks() {

        $admin = new Map_Destination_Admin( $this->version, $this->textdomain );
        $this->loader->add_action(
            'admin_enqueue_scripts',
            $admin,
            'libraries'
        );

        $this->loader->add_action(
            'admin_menu',
            $admin,
            'admin_menu'
        );

        $this->loader->add_action(
            'wp_ajax_save_attractions',
            $admin,
            'save_attractions'
        );

        $this->loader->add_action(
            'wp_ajax_address',
            $admin,
            'get_geocode'
        );
    }


    private function define_user_hooks() {

        $user = new Map_Destination_User( $this->version, $this->textdomain );
        $this->loader->add_action(
            'wp_enqueue_scripts',
            $user,
            'libraries'
        );
        
    }


    /**
     * Build map manager
     * hooks and dependencies
     */
    public function run() {
        $this->loader->run();
    }

}