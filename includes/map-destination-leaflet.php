<?php
/**
 * Module: Map Destination Leaflet
 * Description: Free custom map image
 * from leaflet
 */
class Map_Destination_Leaflet {

    private $version;

    private $textdomain;

    private $loader;
   

    public function __construct( $loader, $version, $textdomain ) {

        $this->version = $version;
        $this->textdomain = $textdomain;        

        if (is_admin()) {
            $loader->add_action(
                'admin_head',
                $this,
                'leatlet_css'
            );
    
            $loader->add_action(
                'admin_footer',
                $this,
                'leatlet_script'
            );
        } else {
            $loader->add_action(
                'wp_head',
                $this,
                'leatlet_css'
            );
    
            $loader->add_action(
                'wp_footer',
                $this,
                'leatlet_script'
            );
        }

    }


    public function leatlet_css() { ?>

        <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
        integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin=""/>

    <?php
    }


    public function leatlet_script() { ?>      
    
        <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
        integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
        crossorigin=""></script>

        <script type="text/javascript">
            leaflet_marker = L.icon({
                iconUrl: '<?php echo MAP_URI ?>/libraries/icons/marker.png',                
                iconSize:     [48, 48],
                iconAnchor:   [24, 48],
                popupAnchor:  [0, -48]
            });
            user_marker = L.icon({
                iconUrl: '<?php echo MAP_URI ?>/libraries/icons/user-marker.png',                
                iconSize:     [48, 48],
                iconAnchor:   [24, 48],
                popupAnchor:  [0, -48]
            });
        </script>

    <?php
    }


    public static function get() {
        return (object) get_option('_map_attractions');
    }


}