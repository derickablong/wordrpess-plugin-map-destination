<?php
/**
 * Management for created
 * attractions
 */
$data = Map_Destination_Leaflet::get();
?>
<div class="wrap">
    <div class="map-destination">    
        <a href="<?php bloginfo('siteurl') ?>/wp-admin/" class="map-button map-button-primary map-wp-dashbaord">WP Dashboard</a>
        <div class="map-panel">
            <div class="map-settings-header">
                <a href="#" class="panel-nav active" data-target="settings">
                    <span class="map-icon icon-settings"></span>
                    Map Settings
                </a>
                <a href="#" class="panel-nav" data-target="location">
                    <span class="map-icon icon-street"></span>
                    Attractions
                </a>
            </div>

            <div class="panel-content map-panel-settings show">  
                <div class="map-field">
                    <h2>Map Settings</h2>
                    <p>Set the center location of the map by providing the address below.</p>
                </div>                
                <div class="map-field">
                    <label for="map-address">Address:</label>            
                    <textarea id="map-address" placeholder="Enter address" style="height:150px"><?php echo trim($data->address) ?></textarea>
                </div>
                <div class="map-field">
                    <p>After setting up the address, click on the map area to plot destinations. Click the added marker on the map to customize content.</p>
                </div>                
                <div class="map-cta">                    
                    <a href="#" class="map-button map-button-primary save-map-settings">Save Settings</a>
                </div>
            </div>  

            <div class="panel-content map-panel-location hide">
                <div class="map-toggle-nav">
                    <a href="#" class="toggle-attractions" data-target="attractions">
                        <h2>Attractions</h2>
                        <p>Display the lists of all the attractions you created.</p>
                    </a>
                    <a href="#" class="toggle-marker" data-target="marker">
                        <h2>Marker Content</h2>
                        <p>Update or remove marker title and content.</p>
                    </a>
                </div>
            </div> 
            
            <div class="map-sub-panel marker hide">
                <div class="map-field map-plot-section">
                    <h2 style="color:#000">Plot Marker</h2>
                    
                    <div class="map-field" style="margin-top: 20px;">
                        <label for="search-address-field">Search and plot by address</label>
                        <textarea id="search-address-field" placeholder="Enter address to plot on the map"></textarea>                        
                    </div>                    
                    <div class="map-field">
                        Or add directly the latitude and longitude below.
                    </div>
                    <div class="map-field">
                        <label for="addr-lat">Latitude:</label>
                        <input type="text" id="addr-lat" value="" placeholder="Enter latitude">
                    </div>
                    <div class="map-field">
                        <label for="addr-lng">Longitude:</label>
                        <input type="text" id="addr-lng" value="" placeholder="Enter longitude">
                    </div>
                    <div class="map-field">
                        <a href="#" class="map-button map-button-primary button-plot-address" style="float:right">Plot Attraction</a>
                    </div>                  
                </div>
                <!-- <div class="map-field map-conditional hide map-cta">
                    <a href="#" class="map-button map-button-danger remove-marker">Remove Marker</a>
                </div> -->
                <div class="map-field map-conditional hide">
                    <h2 style="margin-bottom:20px;color:#000">Marker Content</h2>

                    <label for="location-title">Location Title:</label>
                    <input type="text" id="location-title" placeholder="Enter location title">
                </div>
                <div class="map-field map-conditional hide">
                    <label>Content:</label>
                    <?php
                    $settings = array(
                        'tinymce' => true,
                        'editor_height' => 400,
                        'media_buttons' => true,
                        'drag_drop_upload' => true
                    );
                    wp_editor('', 'map-location-content', $settings);
                    ?>
                </div>
                <div class="map-cta map-conditional hide">                       
                    <a href="#" class="map-button map-button-primary save-location-settings">Save Content</a>
                    <a href="#" class="map-button map-button-danger remove-marker">Remove</a>
                    <a href="#" class="map-button close-marker" style="background:#ccc;color:#000;">Close</a>
                </div>
            </div>

            <div class="map-sub-panel attractions hide">
                <div class="map-field">
                    <h2>Attractions</h2>
                    <p>Click the destination to update or view content.</p>
                    <div style="margin:20px 0;text-align:right;">
                        <a href="#" class="map-button map-button-primary button-add-new-marker">Add New Marker</a>
                    </div>
                </div>
                <div class="map-field map-attractions"></div>
            </div>
        </div>
        <div class="map-canvas" id="map-canvas"></div>
    </div>
    <script type="text/javascript">
        var created_attractions = JSON.parse('<?php echo $data->attractions ?>');
    </script>
</div>