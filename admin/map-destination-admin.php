<?php
/**
 * Module: Map Destination Admin
 * Description: Map management for administrator
 */
class Map_Destination_Admin {

    private $version;

    private $textdomain;

    private $slug;

    public function __construct( $version, $textdomain ) {
        $this->version = $version;
        $this->textdomain = $textdomain;
        $this->slug = 'map-destination';
    }


    public function libraries() {

        wp_enqueue_style(
            'map-destination-admin-css',
            MAP_URI . '/libraries/css/admin.css',
            array(),
            $this->version,
            false
        );

        wp_enqueue_script(
            'map-destination-admin-script',
            MAP_URI . '/libraries/script/admin.js',
            array(),
            $this->version,
            true
        );
        wp_localize_script(
            'map-destination-admin-script',
            'map_api',
            array( 'url' => admin_url( 'admin-ajax.php' ), 'we_value' => 1234 ) 
        );

    }
    
    
    public function admin_menu() {
        add_menu_page(
            __( 'Map Destination', $this->textdomain ),
            __( 'Map Destination', $this->textdomain ),
            'manage_options',
            MAP_DIR . '/admin/page.php',
            '',
            MAP_URI . '/libraries/icons/map.png',
            6
        );
    }


    public function save_attractions() {
        
        $geolocation = $_POST['geolocation'];
        $address     = $_POST['address'];
        $attractions = $_POST['attractions'];


        // clean first
        foreach ($attractions as $index => $attr) {
            if (! isset($attr['title']))
                unset( $attractions[$index] );
        }

        $data        = array(
            'geolocation' => $geolocation,
            'address'     => $address,
            'attractions' => $attractions
        );

        update_option('_map_attractions', $data);

        $response = array(
            'success' => true,
            'attractions' => $attractions
        );

        wp_send_json($response);
        wp_die();
    }


    public function get_geocode() {

        $address = urlencode($_POST['address']);

        $url = "https://maps.googleapis.com/maps/api/geocode/json?address={$address}&key=AIzaSyBG5taMipVd_zJaOZUBZG7fDja2Rx-9gEI&sensor=false";
        
        $resp = json_decode( file_get_contents( $url ), true );

        if ( $resp['status'] === 'OK' ) {			
			$lat   = $resp['results'][0]['geometry']['location']['lat'];
			$long  = $resp['results'][0]['geometry']['location']['lng'];			
		}
        
        wp_send_json(
            array(                
                'latlng' => array( $lat, $long ),
                'url' => $url
            )
        );
        wp_die();

    }


}