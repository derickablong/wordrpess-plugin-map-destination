<?php
/**
 * Module: Map Destination User
 * Description: User interface
 */
class Map_Destination_User {

    private $version;

    private $textdomain;

    private $slug;

    public function __construct( $version, $textdomain ) {
        $this->version = $version;
        $this->textdomain = $textdomain;
        $this->slug = 'map-destination';
        
        add_shortcode( 'map_attractions', array($this, 'map_attractions') );
    }


    public function libraries() {

        wp_enqueue_style(
            'map-destination-user-css',
            MAP_URI . '/libraries/css/user.css',
            array(),
            $this->version,
            false
        );

        wp_enqueue_script(
            'map-destination-user-script',
            MAP_URI . '/libraries/script/user.js',
            array('jquery'),
            $this->version,
            true
        );        

    }


    public function map_attractions( $atts ) {

        $data = Map_Destination_Leaflet::get();

        ob_start();
        ?>

        <div class="map-marker-popup">
            <div class="map-content-wrapper"></div>
        </div>
        <div class="map-canvas" id="map-canvas"></div>
        <input type="hidden" id="map-address" value="<?php echo $data->address ?>">
        <input type="hidden" id="map-geolocation" value="<?php echo $data->geolocation ?>">
        <script type="text/javascript">
            var created_attractions = JSON.parse('<?php echo $data->attractions ?>');
        </script>

        <?php
        $content = ob_get_contents();
        ob_end_clean();


        return $content;
    }

}